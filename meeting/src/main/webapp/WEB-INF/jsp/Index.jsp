<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!doctype html>
<html lang="en" >
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<title>Employee List</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
</head>
<body>
	
	<c:set var="pageListHolder" value="${empList}" scope="session" />
	
	<div class="container my-2">
		
		<h1>Employee List</h1>
		
		<input type="text" id="myInput" onkeyup="searchFunction()"
			placeholder="dueDate filter yyyyy-mm-dd"> 
			
			<ahref="http://localhost:9090/openNewEmpForm"class="btn btn-primary btn-sm mb-3">Add Employee</a>
		
		
		<table border="1" class="table table-striped table-responsive-md">
			<thead>
				<tr>
					<th>S.No.</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Designation</th>
					<th>Type</th>
					<th>Department</th>
					<th>Action</th>
				</tr>
			</thead>

			<tbody id="searchMe">
				<c:forEach var="empInfo" items="${pageListHolder.pageList}">
					<tr>
						<td>${empInfo.id}</td>
						<td>${empInfo.firstName}</td>
						<td>${empInfo.email}</td>
						<td>${empInfo.phone}</td>
						<td>${empInfo.designation}</td>
						<td>${empInfo.type}</td>
						<td>${empInfo.department}</td>
						<td><a
							href="http://localhost:9090/editEmpForm/${empInfo.id}"
							class="btn btn-primary">Edit</a> <a
							href="http://localhost:9090/deleteEmpByid/${empInfo.id}"
							class="btn btn-danger">Delete</a></td>

					</tr>
				</c:forEach>

			</tbody>

		</table>
		<span style="float: left;"> <c:choose>
				<c:when test="${pageListHolder.firstPage}">Prev</c:when>
				<c:otherwise>
					<a href="/all/prev">Prev</a>
				</c:otherwise>
			</c:choose>
		</span> <span> <c:forEach begin="0"
				end="${pageListHolder.pageCount-1}" varStatus="loop">
    			&nbsp;&nbsp;
     			<c:choose>
					<c:when test="${loop.index == pageListHolder.page}">${loop.index+1}</c:when>
					<c:otherwise>
						<a href="/all/${loop.index}">${loop.index+1}</a>
					</c:otherwise>
				</c:choose>
		   		 &nbsp;&nbsp;
		 </c:forEach>
		</span> <span> <c:choose>
				<c:when test="${pageListHolder.lastPage}">Next</c:when>
				<c:otherwise>
					<a href="/all/next">Next</a>
				</c:otherwise>
			</c:choose>
		</span>




	</div>
	<script type="text/javascript">
		function searchFunction() {

			var input, filter, table, tr, td, i, txtValue;
			input = document.getElementById("myInput");
			filter = input.value.toUpperCase();
			table = document.getElementById("searchMe");
			tr = table.getElementsByTagName("tr");

			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[4];
				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}
	</script>
</body>
</html>