<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Add New Emp</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">




</head>

<body>

	<div class="container">

		<h2>Add New Emp</h2>
		<br>



		<form:form action="/saveEmp" modelAttribute="empInfo"
			method="POST">
			<form:hidden path="id" placeholder="id" class="form-control " /> 
			<div class="form-row">
				<div class="form-group col-md-2">
					<label>First Name*</label>
				</div>
				<div class="form-group col-md-4">
					<form:input type="text" path="firstName" placeholder="firstName"
						required="required" class="form-control" />
				</div>
				<div class="form-group col-md-2">
					<label>Last Name*</label>

				</div>
				<div class="form-group col-md-4">
					<form:input type="text" path="lastName" required="required"
						placeholder="lastName" class="form-control"  />
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-2">
					<label>Email*</label>

				</div>
				<div class="form-group col-md-4">
					<form:input type="email" path="email" required="required"
						placeholder="email" class="form-control"  />
				</div>
				<div class="form-group col-md-2">
					<label>Phone*</label>

				</div>

				<div class="form-group col-md-4">
					<form:input type="text" path="phone" placeholder="phone" required="required"
						 class="form-control " />
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-2">
					<label>Designation*</label>

				</div>
				<div class="form-group col-md-4">
					<form:input type="text" path="designation" placeholder="designation" required="required"
						 class="form-control " />
				</div>
				<div class="form-group col-md-2">
					<label>Employment Type*</label>
				</div>
				<div class="form-group col-md-4">
					<form:input type="text" path="type" placeholder="type" required="required"
						 class="form-control " />
				</div>
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-2">
					<label>Department*</label>

				</div>
				<div class="form-group col-md-4">
					<form:input type="text" path="department" placeholder="department" required="required"
						 class="form-control " />
				</div>
				<div class="form-group col-md-2">
					<label hidden>** :</label>
				</div>
				<div class="form-group col-md-4">
					<input type="text" class="form-control" style="visibility: hidden;">
				</div>
			</div>


			

			<input type="submit" value="save" class="btn btn-success"  />
			<button type="clear" class="btn btn-danger">Cancel</button>
		</form:form>
	</div>
	
</body>

</html>
