package com.meetingSchedular.meeting.entity;

import java.sql.Date;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Meeting {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long meetId;
	private String meetingTitle;
	private String colorCode;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDate;
	private String location;
	private EmployeeInfo emp;
	private String meetingLink;
	
	@ManyToOne
    private EmployeeInfo employeeInfo;
	
}
