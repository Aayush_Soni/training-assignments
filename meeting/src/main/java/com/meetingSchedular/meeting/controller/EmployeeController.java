package com.meetingSchedular.meeting.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.meetingSchedular.meeting.entity.EmployeeInfo;
import com.meetingSchedular.meeting.service.EmployeeService;

@Controller
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = { "/all/{type}", "/all" }, method = RequestMethod.GET)
	public ModelAndView all(@PathVariable Map<String, String> pathVariablesMap, HttpServletRequest req) {

		PagedListHolder<EmployeeInfo> list = null;

		String type = pathVariablesMap.get("type");

		if (null == type) {

			List<EmployeeInfo> empList = employeeService.getAllEmployees();

			list = new PagedListHolder<EmployeeInfo>();
			list.setSource(empList);
			list.setPageSize(2);

			req.getSession().setAttribute("empList", list);

//	            printPageDetails(list);

		} else if ("next".equals(type)) {

			list = (PagedListHolder<EmployeeInfo>) req.getSession().getAttribute("empList");

			list.nextPage();

//	            printPageDetails(list);

		} else if ("prev".equals(type)) {

			list = (PagedListHolder<EmployeeInfo>) req.getSession().getAttribute("empList");

			list.previousPage();

//	            printPageDetails(list);

		} else {

			System.out.println("type:" + type);

			list = (PagedListHolder<EmployeeInfo>) req.getSession().getAttribute("empList");

			int pageNum = Integer.parseInt(type);

			list.setPage(pageNum);

//	            printPageDetails(list);
		}

		ModelAndView mv = new ModelAndView("Home");

		return mv;
	}

	@GetMapping("/openNewEmpForm")
	public String openNewEmpForm(Model model) {
		EmployeeInfo employeeInfo = new EmployeeInfo();
		model.addAttribute("employeeInfo", employeeInfo);
		return "new_emp";
	}

	@PostMapping("/saveEmp")
	public String saveEmpInfo(@ModelAttribute("empInfo") EmployeeInfo employeeInfo) {

		employeeService.saveEmployeeInfo(employeeInfo);
		return "redirect:/all";
	}

	@GetMapping("/editEmpForm/{id}")
	public String editEmpForm(@PathVariable(value = "id") long id, Model model) {
		EmployeeInfo employeeInfo= employeeService.getEmployeeById(id);
		model.addAttribute("empInfo", employeeInfo);
		return "update_emp";
	}

	@GetMapping("/deleteEmpByid/{id}")
	public String deleteEmpByid(@PathVariable(value = "id") long id) {
		this.employeeService.deleteEmpById(id);
		return "redirect:/all";
	}

}
