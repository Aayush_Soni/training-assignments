package com.meetingSchedular.meeting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meetingSchedular.meeting.entity.EmployeeInfo;
import com.meetingSchedular.meeting.repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository;

	public List<EmployeeInfo> getAllEmployees() {
		
		return employeeRepository.findAll();
	}

	public void saveEmployeeInfo(EmployeeInfo employeeInfo) {
		this.employeeRepository.save(employeeInfo);
		
	}

	public EmployeeInfo getEmployeeById(long id) {
		EmployeeInfo employeeInfo= employeeRepository.findById(id).get();
		return employeeInfo;
	}

	
	

	public void deleteEmpById(long id) {
		this.employeeRepository.deleteById(id);
		
	}


}
